#!/bin/bash
function split_file() {
str=$1
hyphen=$2
location=$3
IFS=$hyphen     # hyphen (-) is set as delimiter
read -ra ADDR <<< "$str" # str is read into an array as tokens separated by IFS
echo "${ADDR[${location}]}"
IFS=' '   
}

function deploy() {
  mvn deploy:deploy-file -Durl=https://gitlab.com/api/v4/projects/${PROJECT_ID}/packages/maven \
 -DrepositoryId=gitlab-maven \
 -Dfile=$1 \
 -DgroupId=$2 \
 -DartifactId=$3 \
 -Dversion=$4 \
 -Dpackaging=jar \
 -DgeneratePom=true \
 -DgeneratePom.description=$5 \
 -s settings.xml
}

function create_gitlab_deploy() {
    entry=$1
    echo "          ${entry} push:
            stage: build
            image: maven:3.6.2
            before_script:
                - chmod +x ./deploy-script.sh
            variables:
                PROJECT_ID: "26618512" #gitlab project ID
            script:
                - ./deploy-script.sh ${entry}
            only:
              refs:
                - branches
              changes:
                - "${entry}/*"" >> temp.txt
}

FOLDER_NAME=$1
find $FOLDER_NAME -type f -name '*.jar' -print0 | while read -d $'\0' file
do
    echo "file $file"
    groupid=${entry:2}
    artifactid=$(split_file $file '/' 2)
    version=$(split_file $file '/' 3)
    hash=$(split_file $file '/' 4)
    description="File generate for ${artifactid} with version ${version} hash: ${hash}"
    deploy $file $groupid $artifactid $version $description
done  

# for entry in "."/*
# do
#     create_gitlab_deploy "${entry:2}"
#     echo "${entry:2}"
#     find $entry -type f -name '*.jar' -print0 | while read -d $'\0' file
#     do
#         echo "file $file"
#         groupid=${entry:2}
#         artifactid=$(split_file $file '/' 2)
#         version=$(split_file $file '/' 3)
#         hash=$(split_file $file '/' 4)
#         description="File generate for ${artifactid} with version ${version} hash: ${hash}"
#         deploy $file $groupid $artifactid $version $description
#     done  
# done